module.exports = {
    'env': {
        'browser': true,
        'node': true,
        'commonjs': true,
        'es6': true
    },
    'globals': {
        'ENV': true
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'sourceType': 'module',
        'ecmaVersion': 6
    },
    'plugins': [
        'html',
        'json'
    ],
    'rules': {
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'warn',
            'never'
        ]
    }
}
