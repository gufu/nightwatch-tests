let conf = require('../../nightwatch.conf.js')

let elems = {
    title: {
        select: '.bootstrap-select select[data-id="title"]'
    },
    day: {
        select: '.bootstrap-select select[data-id="day"]'
    },
    month: {
        select: '.bootstrap-select select[data-id="month"]'
    },
    year: {
        select: '.bootstrap-select select[data-id="year"]'
    },
    firstName: {
        input: 'input#firstName',
        label: 'label[for="firstName"]'
    },
    lastName: {
        input: 'input#lastName',
        label: 'label[for="lastName"]'
    },
    mobile: {
        input: 'input#mobile',
        label: 'label[for="mobile"]'
    },
    email: {
        input: 'input#email',
        label: 'label[for="email"]'
    },
    emailConfirmation: {
        input: 'input#emailConfirmation',
        label: 'label[for="emailConfirmation"]'
    },
    password: {
        input: 'input#password',
        label: 'label[for="password"]'
    },
    showPassword: {
        input: '.checkbox input#showPassword'
    },
    termsAndConditionsConsent: {
        input: '.checkbox input#termsAndConditionsConsent'
    },
    submit: {
        button: 'button[data-id="submitButton"]'
    }
}

let setValue =  function(sel, value) {
    $(sel).val(value).change();
};

let checkAllElementsInitialSetup = function (browser) {
    browser.expect.element('.container-form').to.be.visible.before(500);

    browser.expect.element('.main-logo').to.have.attribute('alt').not.equals('');
    browser.saveScreenshot(conf.imgpath(browser) + 'eclub-personal-details-step-initial-view.png')

    browser.expect.element(elems.title.select).to.be.present;
    browser.expect.element(elems.title.select + ' option.bs-title-option').to.be.selected;

    browser.expect.element(elems.firstName.input).to.have.attribute('type', 'text');
    browser.expect.element(elems.firstName.label).text.to.contain('*');

    browser.expect.element(elems.lastName.input).to.have.attribute('type', 'text');
    browser.expect.element(elems.lastName.label).text.to.contain('*');

    browser.expect.element(elems.mobile.input).to.have.attribute('type', 'text');
    browser.expect.element(elems.mobile.input).to.have.attribute('pattern', '[0-9]{9}');
    browser.expect.element(elems.mobile.label).text.not.to.contain('*');

    browser.expect.element(elems.day.select).to.be.present;
    browser.expect.element(elems.day.select + ' option.bs-title-option').to.be.present;
    browser.expect.element(elems.day.select + ' option.bs-title-option').to.be.selected;
    browser.expect.element(elems.day.select + ' option[value="1"]').to.be.present;
    browser.expect.element(elems.day.select + ' option[value="17"]').to.be.present;
    browser.expect.element(elems.day.select + ' option[value="31"]').to.be.present;

    browser.expect.element(elems.month.select).to.be.present;
    browser.expect.element(elems.month.select + ' option.bs-title-option').to.be.present;
    browser.expect.element(elems.month.select + ' option.bs-title-option').to.be.selected;
    browser.expect.element(elems.month.select + ' option[value="0"]').to.be.present;
    browser.expect.element(elems.month.select + ' option[value="7"]').to.be.present;
    browser.expect.element(elems.month.select + ' option[value="11"]').to.be.present;

    browser.expect.element(elems.year.select).to.be.present;
    browser.expect.element(elems.year.select + ' option.bs-title-option').to.be.present;
    browser.expect.element(elems.year.select + ' option.bs-title-option').to.be.selected;
    browser.expect.element(elems.year.select + ' option[value="2018"]').to.be.present;
    browser.expect.element(elems.year.select + ' option[value="1999"]').to.be.present;
    browser.expect.element(elems.year.select + ' option[value="1950"]').to.be.present;

    browser.expect.element(elems.email.input).to.have.attribute('type', 'email');
    browser.expect.element(elems.email.label).text.to.contain('*');

    browser.expect.element(elems.emailConfirmation.input).to.have.attribute('type', 'email');
    browser.expect.element(elems.emailConfirmation.label).text.to.contain('*');

    browser.expect.element(elems.password.input).to.have.attribute('type', 'password');
    browser.expect.element(elems.password.label).text.to.contain('*');

    browser.expect.element(elems.showPassword.input).to.have.attribute('type', 'checkbox');
    browser.expect.element(elems.showPassword.input).to.not.be.selected;

    browser.expect.element(elems.termsAndConditionsConsent.input).to.have.attribute('type', 'checkbox');
    browser.expect.element(elems.termsAndConditionsConsent.input).to.not.be.selected;

    // START FILLING OUT THE FORM AND SEE WHAT HAPPENS

    browser.resizeWindow(800, 2000);

    browser.expect.element('.has-error ' + elems.firstName.input).to.not.be.present.before(100);
    browser.expect.element('.has-error ' + elems.lastName.input).to.not.be.present.before(100);
    browser.execute(function () {
        document.querySelector(elems.submit.button).scrollIntoView();
    }, []);
    browser.setValue(elems.email.input, 'invalid@email');
    browser.setValue(elems.password.input, 'p4sSw0rD');
    browser.click(elems.email.label);
    browser.click(elems.showPassword.input);
    browser.waitForElementVisible('.has-error ' + elems.email.input, 1000);
    browser.waitForElementVisible('.has-error ' + elems.password.input, 1000);
    browser.saveScreenshot(conf.imgpath(browser) + 'eclub-validation-errors.png');
    browser.setValue(elems.email.input, 'v.com');
    browser.expect.element('.has-error ' + elems.email.input).to.not.be.present.before(100);
    browser.setValue(elems.password.input, '123!@#');
    browser.expect.element('.has-error ' + elems.email.input).to.not.be.present.before(100);
    browser.saveScreenshot(conf.imgpath(browser) + 'eclub-validation-errors-dismissed.png');
}

module.exports = {

    'Eclub Signup Form': function (browser) {
        browser
            .url('localhost:7070/eclub?lang=en_GB');   // visit the url

        checkAllElementsInitialSetup(browser);

        browser.element('css selector', '.qb-cinema-select button', function (result) {
            if (result.status != -1) { //Element exists, do something
                browser.click('.qb-cinema-select button')
                    .waitForElementVisible('body') // wait for the body to be rendered
            }
        })
        // part two:
        browser
            .saveScreenshot(conf.imgpath(browser) + 'eclub-personal-details-step.png')
            .end()
    }
}
