const getPort = require('get-port')

function makeServer(done) {
    let express = require('express')
    let path = require('path')
    let app = express()

    app.get('/', function (req, res) {
        res.status(200).sendFile('index.html', {root: path.resolve()})
    })
    getPort().then(port => {
        console.log(port)
        let server = app.listen(port, function () {
            done()
        })
        return server
    })
}

module.exports = makeServer